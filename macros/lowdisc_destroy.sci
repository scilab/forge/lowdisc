// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the 
// GNU LGPL license.

function this = lowdisc_destroy (this)
    // Destroy the current object.
    //
    // Calling Sequence
    //   this = lowdisc_destroy (this)
    //
    // Parameters
    //   this: the current object
    //
    // Description
    //   This function requires to take the current object both as an input
    //   and an output argument.
    //
    // Examples
    //   lds = lowdisc_new("faure");
    //   lds
    //   lds = lowdisc_destroy(lds);
    //
    // Authors
    //   Copyright (C) 2013 - Michael Baudin
    //   Copyright (C) 2008-2009 - INRIA - Michael Baudin
    //   Copyright (C) 2010 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "lowdisc_destroy" , rhs , 1:1 )
    apifun_checklhs ( "lowdisc_destroy" , lhs , 0:1 )
    //
    apifun_checktype ( "lowdisc_destroy" , this , "this" , 1 , "LOWDISC" )
    //
    select this.method
    case "niederreiter" then
        this     = ldniedf_destroy ( this )
    case "sobol" then
        this     = ldsobolf_destroy ( this )
    case "faure" then
        this     = ldfauref_destroy ( this )
    case "halton" then
        this     = ldhaltonf_destroy ( this )
    else
        errmsg = msprintf ( gettext ( "%s: Unknown method %s" ) , "lowdisc_destroy" , this.method);
        error(errmsg);
    end
    // Delegate to ldbase
    this.sequence.baseobj = ldbase_destroy ( this.sequence.baseobj )
endfunction

function this = ldsobolf_destroy (this)
    scrambling = lowdisc_cget ( this , "-scrambling" )
    if (scrambling=="") then
        if (this.sequence.token<>-1) then
            _lowdisc_sobolfdestroy ( this.sequence.token )
        end
    else
        if (this.sequence.token<>-1) then
            _lowdisc_ssoboldestroy ( this.sequence.token )
        end
    end
endfunction

function this = ldfauref_destroy (this)
    if (this.sequence.token<>-1) then
        _lowdisc_faurefdestroy ( this.sequence.token )
    end
endfunction

function this = ldhaltonf_destroy (this)
    if (this.sequence.token<>-1) then
        _lowdisc_haltonfdestroy ( this.sequence.token )
    end
endfunction

function this = ldniedf_destroy (this)
    if (this.sequence.token<>-1) then
        _lowdisc_niedfdestroy(this.sequence.token)
    end
endfunction

function this = ldbase_destroy (this)
    this.startedup = %f;
endfunction
