// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

extern "C" {
//#include "stack-c.h" 
#include "Scierror.h"
#include "localization.h"
//#include "gw_lowdisc.h"
#include "liblowdiscgateway.h"
#include "api_scilab.h"
}

/* ==================================================================== */


#include "gw_lowdisc_support.h" 
#include "lowdisc_math.h" 
#include "ssobol.h"
#include "lowdisc_ssobol_map.hxx" 


// token = _lowdisc_ssobolnew ( dim , iflag, coordinate)
// token = _lowdisc_ssobolnew ( dim , iflag, coordinate, seeds )
//   Start the Scrambed Sobol sequence.
// coordinate: a 1-by-1 matrix of boolean, 
//       If false, we must generate all coordinates.
//       If true, we must generate only the dimension-th coordinate.
int sci_lowdisc_ssobolnew (char *fname, void *pvApiCtx) {
	int dimen;
	int ierr;
	int maxd=30;
	int iflag;
	int atmost=1073741823; // The maximum available.
	Ssobol * seq;
	int token;
	int nRows;
	int nCols;
	double *seeds = NULL;
	int isok;
	int coordinate;

	CheckRhs(3,4);
	CheckLhs(0,1);
	// Arg #1 : dimen
	ierr = lowdisc_GetOneIntegerArgument ( fname , 1 , &dimen, pvApiCtx );
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	// Arg #2 : iflag
	ierr = lowdisc_GetOneIntegerArgument ( fname , 2 , &iflag, pvApiCtx );
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	//
	// Get Arg #2: base
	ierr = lowdisc_AssertVariableType(fname , 2 , sci_matrix, pvApiCtx );
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	//
	// Get Arg #3: coordinate (coordinate=1 if false).
	ierr = lowdisc_GetOneBooleanArgument ( fname , 3, &coordinate,pvApiCtx);
	if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
		return 0;
	}
	if ( Rhs == 3 ) 
	{
		// token = _lowdisc_ssobolnew ( dim , iflag )
		seq = new Ssobol(dimen, atmost, iflag, maxd, coordinate, &isok);
	}
	else
	{
		// token = _lowdisc_ssobolnew ( dim , iflag , seeds )
		// Arg #4 : seeds
	        int *piAddr;
		
		getVarAddressFromPosition(pvApiCtx , 4, &piAddr);
		getMatrixOfDouble(pvApiCtx,piAddr,&nRows,&nCols,&seeds);	
		//GetRhsVarMatrixDouble ( 4 , &nRows, &nCols, &seeds);
		if (nRows==24)
		{
			ierr = lowdisc_AssertNumberOfColumns ( fname , 4 , 1 , nCols );
			if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
				return 0;
			}
		}
		else
		{
			ierr = lowdisc_AssertNumberOfRows ( fname , 4 , 1 , nRows );
			if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
				return 0;
			}
			ierr = lowdisc_AssertNumberOfColumns ( fname , 4 , 24 , nCols );
			if ( ierr==LOWDISC_GWSUPPORT_ERROR ) {
				return 0;
			}
		}
		seq = new Ssobol(dimen, atmost, iflag, maxd, coordinate, seeds, &isok);
	}
	// Add the token
	if (isok==1)
	{
		token= lowdisc_ssobol_map_add ( seq );
		lowdisc_CreateLhsInteger ( 1 , token, pvApiCtx );
	}
	else
	{
		lowdisc_CreateLhsInteger ( 1 , -1, pvApiCtx );
	}
	return 0;
}
