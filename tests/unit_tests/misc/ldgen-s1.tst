// Copyright (C) 2013 - Michael Baudin
// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

// Check all sequences in dimension 1
callf = 10;
n = 1;
seqmat = lowdisc_methods ();
for ldseq = seqmat'
  u=lowdisc_ldgen ( callf , n , ldseq );
  assert_checkequal ( size(u) , [callf n] );
  assert_checkequal ( and(u>=0 & u<=1) , %t );
end

